'use strict';

const gulp         = require('gulp');
const notify       = require('gulp-notify');
const twig         = require('gulp-twig'); // Шаблонизатор
const sass         = require('gulp-sass');
const sassGlob     = require('gulp-sass-glob');
const plumber      = require('gulp-plumber'); // Перехват ошибок
const postcss      = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano      = require('cssnano');
const browserSync  = require('browser-sync').create();
const babel        = require('gulp-babel');
const uglify       = require('gulp-uglify');
const imagemin     = require('gulp-imagemin');

// Обработка ошибок
const handleError = err => {
  notify.onError({
    title:   'Gulp error',
    message: err.message
  })(err);
};

// 1. Девсервер на build/
gulp.task('server', () => {
  browserSync.init({
    server: {
      baseDir: 'build/'
    },
    host:   'localhost',
    port:   9000,
    notify: false
  });
});
gulp.task('server:refresh', () => {
  browserSync.reload();
});
gulp.task('server:inject', () => {
  gulp.src('build/styles/**/*.*')
    .pipe(browserSync.stream());
});

// 2. Билды
gulp.task('build:html', () => {
  gulp.src([
      'src/pages/*.twig',
      'src/pages/*.html'
    ])
    .pipe(twig())
    .pipe(gulp.dest('build/'));
});
gulp.task('build:styles', () => {
  gulp.src('src/styles/*.scss')
    .pipe(plumber(handleError))
    .pipe(sassGlob())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({
        browsers: ['last 2 version'],
        grid:     true
      }),
      cssnano({zindex: false, reduceIdents: false})
    ]))
    .pipe(gulp.dest('build/styles/'));
});
gulp.task('build:scripts', () => {
  gulp.src('src/scripts/vendor/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('build/scripts/vendor/'));

  gulp.src(['src/scripts/*.js', '!src/scripts/api.js'])
    .pipe(plumber(handleError))
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(uglify())
    .pipe(gulp.dest('build/scripts/'));

  gulp.src('src/scripts/api.js')
    .pipe(plumber(handleError))
    .pipe(gulp.dest('build/scripts/'));

});
gulp.task('build:assets', () => {
  gulp.src('src/assets/fonts/**/*.*')
    .pipe(gulp.dest('build/assets/fonts/'));

  gulp.src('src/assets/other/**/*.*')
    .pipe(gulp.dest('build/assets/other/'));

  gulp.src('src/assets/img/**/*.*')
    .pipe(imagemin([
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({
        plugins: [
          {removeViewBox: true},
          {convertStyleToAttrs: true},
          {removeUnknownsAndDefaults: true},
          {cleanupIDs: true},
          {cleanupAttrs: true},
          {inlineStyles: true},
          {collapseGroups: true},
          {removeMetadata: true},
          {minifyStyles: true}
        ]
      })
    ]))
    .pipe(gulp.dest('build/assets/img/'));

  gulp.src('src/assets/favicon/**/*.*')
    .pipe(imagemin([
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5})
    ]))

    .pipe(gulp.dest('build/assets/favicon/'));
});

// 3. Вотчеры
gulp.task('watch:build', ['server', 'build:html', 'build:styles', 'build:scripts', 'build:assets'],
  () => {
    gulp.watch([
        'src/pages/**/*.twig',
        'src/pages/**/*.html',
        'src/blocks/**/*.twig',
        'src/blocks/**/*.html'
      ],
      ['build:html']);
    gulp.watch([
        'src/styles/**/*.*',
        'src/blocks/**/*.scss'
      ],
      ['build:styles']);
    gulp.watch('src/scripts/**/*.*', ['build:scripts']);
    gulp.watch('src/assets/**/*.*', ['build:assets']);
  });
gulp.task('watch:update', () => {
  gulp.watch([
    'build/**/*.*',
    '!build/styles/**/*.*'
  ], ['server:refresh']);

  gulp.watch('build/styles/**/*.*', ['server:inject']);
});

gulp.task('default', ['watch:build', 'watch:update']);
gulp.task('build', ['build:html', 'build:styles', 'build:scripts', 'build:assets']);
