module.exports = {
  env: {
    es6: true,
    browser: true,
    jquery: true
  },
  extends: ['airbnb'],
  plugins: ['babel', 'import'],
  parser: 'babel-eslint',
  parserOptions: {ecmaVersion: 6, sourceType: 'module'},
  rules: {
    'no-console': ['error'],
    'arrow-parens': ['error', 'as-needed'],
    'no-multi-spaces': [
      'error',
      {
        exceptions: {
          Property: true,
          VariableDeclarator: true,
          ImportDeclaration: true
        }
      }
    ]
  }
};
